package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/0LuigiCode0/library/logger"
	"github.com/gorilla/mux"
)

// todo mongodb, ts, gorutins

// Block структура блока
type Block struct {
	Index     int64
	TimeStamp time.Time
	Value     int64
	PrevHash  string
	Hash      string
	Nonce     int64
}

type BlockchainModel struct {
	Blocks []*Block
}

var Blockchain []*Block

type Server struct {
	Addr   string
	Pin    string
	router *mux.Router
	log    *logger.Logger
	// wg     *sync.WaitGroup
	mutex *sync.Mutex
}

// RequestData данные блока в запросе
type RequestData struct {
	// Data  string `json:"data"`
	Value int64 `json:"value"`
}

func main() {
	log := logger.InitLogger("")
	if err := InitGenesisBlock(); err != nil {
		log.Fatalf("cannot generate first block: %v", err)
	}
	log.Service("generate genesis block")
	server := InitServer(log)

	if err := server.Start(); err != nil {
		log.Fatalf("server not started: %v", err)
	}
}

func InitGenesisBlock() error {
	genesisBlock := &Block{
		Index:     0,
		TimeStamp: time.Now(),
		Value:     0,
		PrevHash:  "genesis block",
		Nonce:     0,
	}
	genesisBlock.Hash = CalculateHash(genesisBlock)
	Blockchain = append(Blockchain, genesisBlock)
	if err := AddBlockToFile(*genesisBlock); err != nil {
		return err
	}
	return nil
}

func InitServer(logger *logger.Logger) *Server {
	server := &Server{
		Addr:   ":7031",
		Pin:    GetHash("qwerty"),
		router: mux.NewRouter(),
		log:    logger,
	}
	return server
}

func (s *Server) Start() error {
	s.handlerChain()
	s.log.Service("start server with test")
	return http.ListenAndServe(s.Addr, s.router)
}

func (s *Server) handlerChain() {
	s.router = mux.NewRouter()
	s.router.HandleFunc("/get", s.Middleware(s.hGetBlocks))
	s.router.HandleFunc("/gen", s.Middleware(s.hCreateBlock))

	s.log.Service("configure route")
}

// hGetBlocks получение блоков
func (s *Server) hGetBlocks(w http.ResponseWriter, r *http.Request) {
	bytes, err := json.MarshalIndent(Blockchain, "", " ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	io.WriteString(w, string(bytes))
	s.log.Info("get all blocks")
}

// hCreateBlock создание блока
func (s *Server) hCreateBlock(w http.ResponseWriter, r *http.Request) {
	var req RequestData

	decodeBody := json.NewDecoder(r.Body)
	if err := decodeBody.Decode(&req); err != nil {
		s.log.Warningf("cannot decode request json: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot decode request"))
		return
	}
	defer r.Body.Close()

	s.mutex.Lock()
	oldBlock := Blockchain[len(Blockchain)-1]
	newBlock := GenerateBlock(oldBlock, req.Value)

	if !ValidationBlock(*oldBlock, *newBlock) {
		s.log.Errorf("cannot generate new block, oldBlock not valid, index: %v", oldBlock.Index)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot generate new block"))
		return
	}
	newBlockchain := append(Blockchain, newBlock)

	if err := ReplaceChain(newBlockchain); err != nil {
		s.log.Errorf("cannot replace blockchain: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot replace blockchain"))
		return
	}
	s.mutex.Unlock()

	responseBlock, err := json.MarshalIndent(newBlock, "", " ")
	if err != nil {
		s.log.Warningf("cannot responce block: %v\n", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		w.Write([]byte("cannot responce block"))
		return
	}
	if err := AddBlockToFile(*newBlock); err != nil {
		s.log.Errorf("cannot add block to file: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		w.Write([]byte("cannot add block to file"))
	}

	w.WriteHeader(http.StatusCreated)
	w.Write(responseBlock)
	s.log.Infof("generate new block; index: %v", newBlock.Index)
}

func (s *Server) Middleware(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if GetHash(r.FormValue("pin")) != s.Pin {
			s.log.Warning("pin not valid")
			http.Error(w, "pin not valid", http.StatusForbidden)
			return
		}
		handler(w, r)
	}
}

func GetHash(data string) string {
	h := sha256.New()
	h.Write([]byte(data))
	return string(h.Sum(nil))
}

// AddBlockToFile добавить блок в файл
func AddBlockToFile(block Block) error {
	file, err := os.OpenFile("bch.db", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	blockJson, err := json.Marshal(block)
	if err != nil {
		return err
	}
	data := string(blockJson) + "\n"
	if _, err = file.WriteString(string(data)); err != nil {
		return err
	}
	return nil
}

// GenerateBlock генерация блока
func GenerateBlock(old *Block, value int64) *Block {
	newBlock := new(Block)
	newBlock.Index = old.Index + 1
	newBlock.TimeStamp = time.Now()
	newBlock.PrevHash = old.Hash
	newBlock.Value = value
	newBlock.Hash = CalculateHash(newBlock)
	return newBlock
}

// CalculateHash создает хеш из данных блока
func CalculateHash(block *Block) string {
	recordData := fmt.Sprintf("%v%v%v%v", strconv.Itoa(int(block.Index)), block.TimeStamp.String(), strconv.Itoa(int(block.Value)), block.PrevHash)
	hash := sha256.New()
	hash.Write([]byte(recordData))
	hashed := hash.Sum(nil)

	return hex.EncodeToString(hashed)
}

// BlockValidation проверка блока
func ValidationBlock(oldBlock, newBlock Block) bool {
	if oldBlock.Hash != newBlock.PrevHash {
		return false
	}
	if oldBlock.Index+1 != newBlock.Index {
		return false
	}
	if len(Blockchain) > 2 {
		if CalculateHash(&oldBlock) != oldBlock.Hash {
			return false
		}
		if CalculateHash(&newBlock) != newBlock.Hash {
			return false
		}
	}
	return true
}

// ReplaceChain выбираем самую длинную цепочку блоков, как самую актуальную
func ReplaceChain(newChain []*Block) error {
	if len(newChain) > len(Blockchain) {
		Blockchain = newChain
		return nil
	}
	return fmt.Errorf("not replace blockchain")
}
